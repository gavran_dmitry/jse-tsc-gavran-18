package ru.tsc.gavran.tm.service;

import ru.tsc.gavran.tm.api.service.ILogService;

import java.util.logging.*;
import java.io.IOException;
import java.io.InputStream;

public class LogService implements ILogService {

    private final static String FILE_NAME = "/logger.properties";

    private final static String COMMANDS = "COMMANDS";
    private final static String COMMANDS_FILE = "./commands.txt";

    private final static String MESSAGES = "MESSAGES";
    private final static String MESSAGES_FILE = "./messages.txt";

    private final static String ERRORS = "ERRORS";
    private final static String ERRORS_FILE = "./errors.txt";

    private final ConsoleHandler consoleHandler = new ConsoleHandler();
    private final LogManager manager = LogManager.getLogManager();
    private final Logger root = Logger.getLogger("");
    private final Logger commands = Logger.getLogger(COMMANDS);
    private final Logger messages = Logger.getLogger(MESSAGES);
    private final Logger errors = Logger.getLogger(ERRORS);

    {
        init();
        registry(errors, ERRORS_FILE, true);
        registry(commands, COMMANDS_FILE, false);
        registry(messages, MESSAGES_FILE, true);
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void registry(final Logger logger, final String filename, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void init() {
        try {
            final InputStream inputStream = LogService.class.getResourceAsStream(FILE_NAME);
            manager.readConfiguration(inputStream);
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void debug(String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

    @Override
    public void command(String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void error(Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
