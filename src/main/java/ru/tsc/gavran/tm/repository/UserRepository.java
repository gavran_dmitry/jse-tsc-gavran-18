package ru.tsc.gavran.tm.repository;

import ru.tsc.gavran.tm.api.repository.IUserRepository;
import ru.tsc.gavran.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User add(User user) {
        users.add(user);
        return user;
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Override
    public User findById(String id) {
        for (User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(String login) {
        for (User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        return null;
    }

    @Override
    public User remove(User user) {
        return null;
    }

    @Override
    public User removeUserById(String id) {
        return null;
    }

    @Override
    public User removeUserByLogin(String login) {
        return null;
    }
}
