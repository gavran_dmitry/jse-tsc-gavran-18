package ru.tsc.gavran.tm.command.user;

import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.exception.entity.UserNotFoundException;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.util.TerminalUtil;

public class UserShowByIdCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-show-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show user by id.";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth) throw new AccessDeniedException();
        System.out.println("ENTER USER ID: ");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (!isAdmin) {
            if (!currentUserId.equals(user.getId())) throw new AccessDeniedException();
        }
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

}