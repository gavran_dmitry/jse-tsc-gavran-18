package ru.tsc.gavran.tm.api.repository;

import ru.tsc.gavran.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    void clear();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    User removeUserById(String id);

    User removeUserByLogin(String login);

}
