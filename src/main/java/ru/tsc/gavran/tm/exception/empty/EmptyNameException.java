package ru.tsc.gavran.tm.exception.empty;

import ru.tsc.gavran.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error. Name is empty");
    }

    public EmptyNameException(String value) {
        super("Error." + value + " Name is empty");
    }

}
