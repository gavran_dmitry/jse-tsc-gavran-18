package ru.tsc.gavran.tm.util;

import ru.tsc.gavran.tm.exception.AbstractException;
import ru.tsc.gavran.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws AbstractException {
        final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (RuntimeException e) {
            throw new IndexIncorrectException(value);
        }
    }

    static void incorrectValue() {
        System.out.println("Incorrect value");
    }

}